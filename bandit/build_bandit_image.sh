#/bin/bash
set -x

# Login to DockerHub before running it
# docker login

DOCKER_REPO="jaynegc"
IMAGE_NAME="python_bandit"
CURR_DIR=`pwd`

function clean_build_and_publish_image {
	PYTHON_VERSION=$1
	IMAGE_TAG="${PYTHON_VERSION}-latest"
	cd $CURR_DIR/Python${PYTHON_VERSION}

	# Remove all images
	set +e
	docker rmi $(docker images -a -q | grep ${IMAGE_NAME})
	set -e

	# Build and publish image
	docker build -t ${DOCKER_REPO}/${IMAGE_NAME}:${IMAGE_TAG} .
	echo "[SUCCESS] Image built!"
	#docker push ${DOCKER_REPO}/${IMAGE_NAME}:${IMAGE_TAG}
	echo "[SUCCESS] Image published!"

	cd $CURR_DIR
}

# Building with Python 2
clean_build_and_publish_image "2.7.15"

# Building with Python3
clean_build_and_publish_image "3.7.2"

# Cleanup all garbage
docker system prune -a
docker volume prune
echo "[SUCCESS] All done!"
