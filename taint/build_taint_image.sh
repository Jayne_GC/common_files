#/bin/bash
set -x

# Login to DockerHub before running it
# docker login

DOCKER_REPO="jaynegc"
IMAGE_NAME="python_taint"
IMAGE_TAG="latest"

# Remove all images
docker rmi $(docker images -a -q | grep ${IMAGE_NAME})
set -e

# Build and publish image
docker build -t ${DOCKER_REPO}/${IMAGE_NAME}:${IMAGE_TAG} .
echo "[SUCCESS] Image built!"
docker push ${DOCKER_REPO}/${IMAGE_NAME}:${IMAGE_TAG}
echo "[SUCCESS] Image published!"

# Cleanup all garbage
docker system prune -a
docker volume prune
echo "[SUCCESS] All done!"
