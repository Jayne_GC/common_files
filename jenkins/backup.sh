# Jenkins Configuraitons Directory
set -e

USER=`whoami`
if [ $USER == "jenkins" ]; then
    JENKINS_HOME=/var/jenkins_home
else
    JENKINS_HOME=/data/jenkins
fi
cd $JENKINS_HOME

# Add general configurations, job configurations, and user content
git add -- *.xml jobs/*/* userContent/* plugins/* users/*.xml

# only add user configurations if they exist
if [ -d users ]; then
    user_configs=`ls users/*/config.xml`

    if [ -n "$user_configs" ]; then
        git add $user_configs
    fi
fi

git commit -m "Automated Jenkins commit"

git push
