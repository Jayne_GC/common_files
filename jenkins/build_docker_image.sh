#/bin/bash
set -x

# Login to DockerHub before running it
# docker login

DOCKER_REPO="jaynegc"
IMAGE_NAME="alpine_jenkins_docker"
IMAGE_TAG="latest"

# Remove all images
docker rmi $(docker images -a -q | grep ${IMAGE_NAME})
set -e

# Build and publish image
docker build -t ${DOCKER_REPO}/${IMAGE_NAME}:${IMAGE_TAG} .
echo "[SUCCESS] Image built!"
docker push ${DOCKER_REPO}/${IMAGE_NAME}:${IMAGE_TAG}
echo "[SUCCESS] Image published!"

# Stop and Clean all containers
set +e
docker stop $(docker ps -a -q)
docker rm -v $(docker ps -a -q)
rm -rf /data/jenkins/*
rm -rf /data/jenkins/.java
rm -rf /data/jenkins/.cache
set -e

# Start container
docker-compose up -d
echo "[SUCCESS] Container started!"

# Cleanup all garbage
docker system prune -a
docker volume prune

# List container
docker ps -a
echo "[SUCCESS] All done!"
