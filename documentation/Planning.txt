Main
Monitors opensource and send changelog to child job
Calls 2 jobs

Job 1

Step 1 - Clone open code in workspace
Step 4 - Deploy code in a container
Step 5 - Run app tests

Job 2

Step 1 - Clone open code in workspace
Step 2 - Docker compose volume workspace and run pysec
Step 3 - Publish result from volume workspace somewhere and run analysis - alert - with changelog
Step 4 - Deploy code in a container
Step 5 - Run app tests

Similar apps:
https://geekflare.com/find-python-security-vulnerabilities/

Pysec is a re-implementation of python to avoid issues
https://belitsoft.com/assets/python-security.pdf
http://www.pythonsecurity.org/
https://bitbucket.org/Jayne_GC/common_files/src/master/pysec/
https://cloud.docker.com/repository/docker/jaynegc/python_security

- Python Taint
https://github.com/python-security/pyt
https://cloud.docker.com/repository/docker/jaynegc/python_taint
https://bitbucket.org/Jayne_GC/common_files/src/master/taint/
- Pyntch
http://www.unixuser.org/~euske/python/pyntch/index.html
- Bandit
https://github.com/PyCQA/bandit
- Spaghetti
https://www.andreafortuna.org/cybersecurity/spaghetti-a-python-web-application-security-scanner/
https://github.com/m4ll0k/WAScan




Mention https://github.com/m4ll0k/WAScan as an assitant to blackbox tests


Source
https://github.com/pandas-dev/pandas - size 
https://github.com/ageitgey/face_recognition
https://github.com/ansible/ansible

 
https://github.com/ageitgey/face_recognition.git
https://github.com/pandas-dev/pandas.git
https://github.com/scikit-learn/scikit-learn.git
https://github.com/django/django.git
https://github.com/matplotlib/matplotlib.git
https://github.com/sympy/sympy.git


Print the end of results on jenkins
publish in a git repo the full log
fail the job if any high sev

comment on ever new commit analysys - do a job for it that calls the pipeline








