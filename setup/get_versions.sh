# Get versions

git --version
docker --version
docker-compose --version

# git version 2.19.1
# Docker version 18.06.1-ce, build e68fc7a
# docker-compose version 1.21.0, build unknown

